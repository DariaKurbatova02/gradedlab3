//Daria Kurbatova
//2043577
package com.example;
/**
 * Test is a class to test the functioning of Vector3d.java class
 * @author Daria Kurbatova
 * @version 9/08/2022
 */
public class Test {
    /**
     * tests the vectors
     */
    public static void main(String[] args){
        //first vector
        System.out.println("first vector:");
        Vector3d a = new Vector3d(34, 2, 7);
        System.out.println(a);

        //second vector
        System.out.println("second vector:");
        Vector3d b = new Vector3d(2, 3, 5);
        System.out.println(b);

        //testing the magnitude method
        double aMagnitude = a.magnitude();
        System.out.println("Vector a's magnitude:");
        System.out.println(aMagnitude);

        //testing the dotProduct method
        double abDotProduct = a.dotProduct(b);
        System.out.println("a and b's dot product:");
        System.out.println(abDotProduct);

        //testing the sum method
        Vector3d c = a.add(b);
        System.out.println("sum vector of a and b");
        System.out.println(c);
        
    }
    
}
