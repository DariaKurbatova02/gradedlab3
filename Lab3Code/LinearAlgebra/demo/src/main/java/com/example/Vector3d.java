//Daria Kurbatova
//2043755
package com.example;
import java.lang.Math;
/**
 * Vector3d is a class used to create vectors and perform operations on them
 * information about vectors
 * @author Daria Kurbatova
 * @version 9/8/2022
 */
public class Vector3d {
    private double x;
    private double y;
    private double z;

    /**
     * initialize the vector
     * @param x first vector value
     * @param y second vector value
     * @param z third vector value
     */
    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    /**
     * get method for x
     * @return the value of x
     */
    public double getX(){
        return this.x;
    }
    /**
     * get method for y
     * @return the value of y
     */
    public double getY(){
        return this.y;
    }
    /**
     * get method for z
     * @return the value of z
     */
    public double getZ(){
        return this.z;
    }
    /**
     * calculates the magnitude of this vector
     * @return the magnitude of this vector
     */
    public double magnitude(){
        double magnitude = Math.sqrt((this.x*this.x)+(this.y*this.y)+(this.z*this.z));
        return magnitude;
    }
    /**
     * calculates the dot product of this vector and another vector
     * @param vector the second vector used to calculate the dot product with this vector
     * @return the value of the dot product of the two vectors
     */
    public double dotProduct(Vector3d vector){
        double dotProduct = ((this.x*vector.getX())+(this.y*vector.getY())+(this.z*vector.getZ()));
        return dotProduct;
    }
    /**
     * sum this vector with another vector
     * @param vector vector to be added to this vector
     * @return sum of this vecor with inputed vecor
     */
    public Vector3d add(Vector3d vector){
        double xSum = (this.x+vector.getX());
        double ySum = (this.y+vector.getY());
        double zSum = (this.z+vector.getZ());
        Vector3d summedVector = new Vector3d(xSum, ySum, zSum);
        return summedVector;
    }
    /**
     * create string representation of vector
     * @return string representation of this vector
     */
    public String toString(){
        return " x: "+this.x+"\n y: "+this.y+"\n z: "+this.z;
    }
}
