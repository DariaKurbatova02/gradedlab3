//Daria Kurbatova
//2043755
package com.example;

import java.lang.Math;
import static org.junit.Assert.*;
import org.junit.Test; 


public class Vector3dTests {
    @Test
    public void testGetMethodForX(){
        //testing X get method
        Vector3d a = new Vector3d(1, 2, 3);
        Double expectedValue = 1.0;
        Double actualValue = a.getX();
        assertEquals(expectedValue, actualValue, 0);
    }
    @Test
    public void testGetMethodForY(){
        Vector3d a = new Vector3d(1, 2, 3);
        double expectedValue = 2.0;
        double actualValue = a.getY();
        assertEquals(expectedValue, actualValue, 0);
    }
    @Test
    public void testGetMethodForZ(){
        Vector3d a = new Vector3d(1, 2, 3);
        double expectedValue = 3.0;
        double actualValue = a.getZ();
        assertEquals(expectedValue, actualValue, 0);
    }
    @Test
    public void testMagnitude(){
        Vector3d a = new Vector3d(2, 3, 5);
        double expectedValue = Math.sqrt((a.getX()*a.getX())+(a.getY()*a.getY())+(a.getZ()*a.getZ()));
        double actualValue = a.magnitude();
        assertEquals(expectedValue, actualValue, 0);
    }
    @Test
    public void testDotProduct(){
        Vector3d a = new Vector3d(2, 1, 3);
        Vector3d b = new Vector3d(1, 5, 7);
        double expectedDotProduct = ((a.getX()*b.getX())+(a.getY()*b.getY())+(a.getZ()*b.getZ()));
        double actualDotProduct = (a.dotProduct(b));
        assertEquals(expectedDotProduct, actualDotProduct, 0);
    }
    @Test
    public void testAdd(){
        Vector3d a = new Vector3d(7, 3, 2);
        Vector3d b = new Vector3d(11, 17, 3);
        Vector3d actualVector = a.add(b);
        assertEquals(18, actualVector.getX(), 0);
        assertEquals(20, actualVector.getY(), 0);
        assertEquals(5, actualVector.getZ(), 0);
    }  
}
